#![no_std]
#![no_main]

use ghash::*;

psp::module!("ghash", 1, 0);

fn psp_main() {
    psp::enable_home_button();
    let input = "".as_bytes();
    psp::dprintln!("input: {:?}", input);
    psp::dprintln!("BLAKE-64\t{:}", Blake64::default().hash_to_lowerhex(input));
    psp::dprintln!(
        "BLAKE-512\t{:}",
        Blake512::default().hash_to_lowerhex(input)
    );
    psp::dprintln!(
        "BLAKE2s-256\t{:}",
        Blake2s::default().hash_to_lowerhex(input)
    );
    psp::dprintln!(
        "BLAKE2b-512\t{:}",
        Blake2b::default().hash_to_lowerhex(input)
    );
    psp::dprintln!(
        "EDON-R512\t{:}",
        EdonR512::default().hash_to_lowerhex(input)
    );
    psp::dprintln!(
        "Keccak-512\t{:}",
        Keccak512::default().hash_to_lowerhex(input)
    );
    psp::dprintln!("MD2:\t\t{:}", Md2::default().hash_to_lowerhex(input));
    psp::dprintln!("MD4:\t\t{:}", Md4::default().hash_to_lowerhex(input));
    psp::dprintln!("MD5:\t\t{:}", Md5::default().hash_to_lowerhex(input));
    psp::dprintln!(
        "RIPEMD-320:\t{:}",
        Ripemd320::default().hash_to_lowerhex(input)
    );
    psp::dprintln!("SHA-1:\t\t{:}", Sha1::default().hash_to_lowerhex(input));
    psp::dprintln!("SHA-512:\t{:}", Sha512::default().hash_to_lowerhex(input));
    psp::dprintln!(
        "SHA3-512:\t{:}",
        Sha3_512::default().hash_to_lowerhex(input)
    );
}

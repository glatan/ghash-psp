# Ghash-PSP

[Ghash](http://www.gitlab.com/glatan/ghash)のPSP向けビルド。

![Screenshot](./Screenshot.jpg)

## Build

## 準備

```sh
# Install cargo-psp
cargo install cargo-psp
# Clone repository
git clone https://gitlab.com/glatan/ghash-psp.git
cd ghash-psp
# Build
cargo-psp
```

## 実行

PPSSPPかCFW導入済みのPSP実機で`target/mipsel-sony-psp/debug/EBOOT.PBP`を実行してください。
